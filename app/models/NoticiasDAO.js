function NoticiasDAO(connection){
    this._connection = connection;
}
/**
 * GET NOTICIAS [ PLURAL ]
 * Método que retorna todas as noticias
 */
NoticiasDAO.prototype.getNoticias = function( callback ){
    this._connection.query('select * from noticias order by data_noticia desc ', callback);
}

/**
 * GET NOTICIA [ SINGULAR ]
 * Método que retorna conteúdo de uma noticia somente
 * 
 * @param callbal
 */
NoticiasDAO.prototype.getNoticia = function( id_noticia, callback ){
    var id = id_noticia.id_noticia;
    this._connection.query('select * from noticias where id_noticia = ' + id , callback );
}

/**
 * SALVAR NOTICIA
 * Método para salvar dados no BD
 * 
 * @param noticia traz um JSON (Array de infos assim por dizer)
 * @param callback 
 */
NoticiasDAO.prototype.salvarNoticia = function( noticia, callback ){
    console.log('Todos os dados foram salvos com sucesso : noticia');
    this._connection.query('insert into noticias set ? ', noticia, callback );
}

/**
 * GET 5 ULTIMAS NOTICIAS
 * Função que retorna as 5 ultimas noticias adicionadas no BD
 * 
 * @param callback
 */
NoticiasDAO.prototype.get5UltimasNoticias = function( callback ){
    this._connection.query('select * from noticias order by data_criacao desc limit 5', callback);
}


// EXPORT DA CLASSE
module.exports = function ( app ) {
    return NoticiasDAO;
}// fim da function