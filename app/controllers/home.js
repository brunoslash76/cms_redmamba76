module.exports.index = function(app, req, res){

    var connection = app.config.db_connection();
    var noticiasDAO = new app.app.models.NoticiasDAO(connection);

    noticiasDAO.get5UltimasNoticias(function(error, result){
        res.render("home/index", {noticias : result});
    });
}// fim da func